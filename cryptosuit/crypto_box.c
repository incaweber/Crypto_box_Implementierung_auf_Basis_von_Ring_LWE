#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "randombytes.h"


#ifndef TYPES 
#define TYPES

#include <inttypes.h>

#define MOD 7681 /* modulus of the ring */
#define GEN 17   /* generator in the ring */

//NOTE: only edit these together!
#define DOUBLE_DIM 512  /* two times dimension of the ring */
#define DOUBLE_INV 7666 /* multiplicative inverse of the doubled dimension modulo MOD */
#define DIM 256  /* dimension of the ring */
#define INV 7651 /* multiplicative inverse of the dimension modulo MOD */

/**
 * @brief 	   {holds one ring element (private key is a ring element)}
 */
typedef struct poly { int32_t coeff[DIM]; } poly;

/**
 * @brief      {holds a public key of the form: (a, b = a*s+e)}
 */
typedef struct tuple {
	poly a;
	poly b;
} tuple;

#endif

/**
 * @brief      { holds ring element with double dimension.
 * 				 this is used for elementwise multiplication with ntt.
 * 				 The polynomials get padded with zeros in the front in a 
 * 				 way that allows us to calculate the 2n-1 sized product of
 * 				 two polynomials }
 */
typedef struct double_poly { int32_t coeff[2*DIM]; } double_poly;

#define FOR(i,n) for (int i = 0; i < DIM; ++i)

//gaussian params
#define TAILCUT 37
#define GAUSSIAN 11.31
#define MANTISSA 52
#define BIAS 1023
#define EXPONENT 0x07FF
#define PRECISION 55
#define MAX_GAUSS 30

#ifdef MY_DEBUG

#include <stdio.h>

void print_poly(poly a) {
	FOR(i,n) {
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}

void print_double_poly(double_poly a) {
	for (int i = 0; i < DOUBLE_DIM; ++i)
	{
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}

#endif

/**
 * @brief      Modulo function with fixed modulus MOD (@see types.h)
 * 
 * Warning this is really inefficient. Only for use of very close numbers.
 * This is only used in subtractions of field elements.
 *
 * @param[in]  a     { value to calculate modulus of }
 *
 * @return     { returns representative 0 <= x < MOD of the coset of a }
 */
int32_t modulo(int32_t a) {

	while (a < 0) {
		a += MOD;
	}

	a = a % MOD;

	return a;
}


/**
 * @brief      naive implementation of modular exponentiation
 *
 * @param[in]  a     { base }
 * @param[in]  b     { exponent }
 *
 * @return     { return a^b mod MOD (@see types.h) }
 */
int32_t power(int32_t a, int32_t b) {

	if (b == 0) return 1;
	if (b == 1) return a;

	int x = a;
	//calculates a^b mod c
	while (--b > 0) {
		x = x*a % MOD;
	}

	return x;
}



/**
 * @brief      recursion helper for NTT calculation of a poly
 *
 * @param      result  { gets step-by-step filled with the evaluated polynomial}
 * @param      a       { polynomial to calculate ntt of }
 * @param[in]  n       { arraysizes }
 */
void forward_recursive_ntt(int32_t result[], int32_t a[], int n) {

	//base case:
	if (n == 1) {
		result[0] = a[0];
		return;
	}

	//calculate the n-th root of unity.
	//note that (MOD-1)/n is always an int because by requirement we have that:
	//MOD = 1 mod 2*DIM
	//and DIM is a power of 2
	int32_t root = power(GEN, (int32_t)(MOD-1)/n);
	int32_t omega = 1;


	//split a into two pieces
	//where a0 contains all even indices
	//and a1 contains all odd indices	
	int half = n/2;
	int32_t a0[half];
	int32_t a1[half];
	int32_t res0[half];
	int32_t res1[half];

	for (int i = 0; i < n; ++i) {
		int tmp = ceil(i/2);
		if (i % 2) {
			a1[tmp] = a[i];
		} else {
			a0[tmp] = a[i];
		}
	}

	//recursive case:
	forward_recursive_ntt(res0, a0, half);
	forward_recursive_ntt(res1, a1, half);

	//coming out of the recursion and doing the calculations
	for (int i = 0; i < half; ++i) {
		result[i] = (res0[i] + omega*res1[i]) % MOD;
		result[i+half] = modulo(res0[i] - ((omega*res1[i]) % MOD));
		omega = omega*root % MOD;
	}
}

/**
 * @brief      recursion helper for calculating the inverse ntt
 *
 * @param      result  { gets step-by-step filled with polynomial }
 * @param      a       { polynomial to calculate inverse ntt of }
 * @param[in]  n       { arraysizes }
 */
void inverse_recursive_ntt(int32_t result[], int32_t a[], int n) {

	//base case:
	if (n == 1) {
		result[0] = a[0];
		return;
	}

	//calculate the inverse of the n-th root of unity.
	//note that a^-1 = a^(n-1)
	int32_t root = power(GEN, (int32_t)(MOD-(MOD-1)/n)-1);
	int32_t omega = 1;

	//split a into two pieces 
	//where a0 contains all even indices
	//and a1 contains all odd indices
	int half = n/2;
	int32_t a0[half];
	int32_t a1[half];
	int32_t res0[half];
	int32_t res1[half];

	for (int i = 0; i < n; ++i) {
		int tmp = ceil(i/2);
		if (i % 2) {
			a1[tmp] = a[i];
		} else {
			a0[tmp] = a[i];
		}
	}

	//recursive case:
	inverse_recursive_ntt(res0, a0, half);
	inverse_recursive_ntt(res1, a1, half);

	//coming out of the recursion and doing the calculations
	for (int i = 0; i < half; ++i) {				
		result[i] = (res0[i] + omega*res1[i]) % MOD;
		result[i+half] = modulo(res0[i] - ((omega*res1[i]) % MOD));
		omega = omega*root % MOD;
	}
}

poly ntt(poly a) {

	poly res;
	forward_recursive_ntt(res.coeff, a.coeff, DIM);
	return res;
}

poly inv_ntt(poly a) {

	poly res;
	inverse_recursive_ntt(res.coeff, a.coeff, DIM);
	FOR(i,n) res.coeff[i] = res.coeff[i] * INV % MOD;
	return res;
}

double_poly double_ntt(double_poly a) {

	double_poly res;
	forward_recursive_ntt(res.coeff, a.coeff, DOUBLE_DIM);
	return res;
}

double_poly double_inv_ntt(double_poly a) {

	double_poly res;
	inverse_recursive_ntt(res.coeff, a.coeff, DOUBLE_DIM);
	for (int i = 0; i < DOUBLE_DIM; ++i) 
		res.coeff[i] = (res.coeff[i] * DOUBLE_INV) % MOD;
	return res;
}

/**
 * @brief      Ringaddition (Z_MOD^DIM[x]/(x^DIM+1))
 *
 * @param[in]  x1    summand 1
 * @param[in]  x2    summand 2
 *
 * @return     { sum of x1 and x2 modul }
 */
poly __add_poly(poly x1, poly x2) {
	poly res;
	FOR(i,n) res.coeff[i] = (x1.coeff[i] + x2.coeff[i]) % MOD;
	return res;
}

/**
 * @brief      Ringmultiplication (Z_MOD^DIM[x]/(x^DIM+1))
 *
 * Multiplicates two ring elements x1 and x2 using NTT that evaluates the 
 * polynomials in 2*DIM points - the roots of unity -, multiplicates them
 * component-wise then transforms them back and reduces them back to the ring.
 *
 * @param[in]  x1    factor 1
 * @param[in]  x2    factor 2
 *
 * @return     { product (in the ring of x1 and x2) }
 */
poly __mult_poly(poly x1, poly x2) {
	
#ifdef MY_DEBUG

printf("\n__mult_poly with: x1,x2:\n");
print_poly(x1);
print_poly(x2);

#endif

	poly result;
	double_poly ntt1;
	double_poly ntt2;
	double_poly ntt_mul; 

	//padding with zeroes for multiplication:
	for (int i = 0; i < DOUBLE_DIM; ++i) {
		ntt1.coeff[i] = (i<DIM)?0:x1.coeff[i-DIM];
		ntt2.coeff[i] = (i<DIM)?0:x2.coeff[i-DIM];
	}

#ifdef MY_DEBUG

printf("\nPadded x1,x2:\n");
print_double_poly(ntt1);
print_double_poly(ntt2);

#endif

	//NTT-Transformation
	ntt1 = double_ntt(ntt1);
	ntt2 = double_ntt(ntt2);

#ifdef MY_DEBUG

printf("\nTransformed x1,x2:\n");
print_double_poly(ntt1);
print_double_poly(ntt2);

#endif


	//ntt does not give ring elements in the same ring with double dimension
	//so no negative values => can use built in mod
	for (int i = 0; i < DOUBLE_DIM; ++i) {
		ntt_mul.coeff[i] = (ntt1.coeff[i] * ntt2.coeff[i]) % MOD;
	}

#ifdef MY_DEBUG

printf("\nCoefficient-wise multiplication:\n");
print_double_poly(ntt_mul);

#endif

	//Inverse NTT-Transformation
	ntt_mul = double_inv_ntt(ntt_mul);

#ifdef MY_DEBUG

printf("\nInverse transformed:\n");

print_double_poly(ntt_mul);

#endif

	//reducing result mod (x^MOD + 1)
	//NOTE: the multiplication puts the leading zero we have to ignore
	//to the last index (DOUBLE_DIM-1)
	result.coeff[0] = ntt_mul.coeff[DIM-1];
	for (int i = 0; i < DIM-1; ++i) 
		result.coeff[i+1] = modulo(ntt_mul.coeff[i+DIM] - ntt_mul.coeff[i]);

#ifdef MY_DEBUG

printf("\nReduced:\n");
print_poly(result);

#endif

	return result;
}

/**
 * @brief      additional inverse
 * 
 * Calculates the additional inverse of a ring element, s.t.: 
 * __add_poly(x1, __add_inverse(x1)) = [0].
 *
 * @param[in]  x1    the poly to invert
 */
poly __add_inverse(poly x1) {

	poly res;
	FOR(i,n) res.coeff[i] = (x1.coeff[i] == 0)?0:MOD - x1.coeff[i];
	return res;
}

/**
 * This is the columnwise representation of the probability matrix. It consits of 
 * PRECISION (gaussian_params.h) 64-bit integers that each represent one column.
 * We process it bit-wise. Each bit represents a terminal node in the DDG-Tree of
 * the Knuth-Yao Algorithm. The deepest bit used in every int is TAILCUT 
 * (gaussian_params.h)
 */
uint64_t prob_matrix[PRECISION] = 
	   {0x0000000000000000,0x0000000000000000,0x7000000000000000,0x8E00000000000000,
	    0x6D80000000000000,0xD940000000000000,0x8A70000000000000,0x21A8000000000000,
	    0x8B24000000000000,0x14BA000000000000,0xC6E5000000000000,0x075E800000000000,
	    0x7C03400000000000,0x0D87A00000000000,0xDFA7000000000000,0x4E58D00000000000,
	    0xF594D80000000000,0x5C08000000000000,0x60C91C0000000000,0x0166460000000000,
	    0x2520A40000000000,0xBCD5E30000000000,0x0612358000000000,0x0B67C00000000000,
	    0xFED9BD4000000000,0x570F430000000000,0xF5850EE000000000,0x5F6D700000000000,
	    0x100F563000000000,0x31C5A43000000000,0x9214E56800000000,0x9698E2C800000000,
	    0x9D3490F400000000,0xB65AC52C00000000,0x8F5BFC5200000000,0x45CFF65600000000,
	    0x04D1B37900000000,0x42AE5C4800000000,0x796A66F180000000,0x520EC2B000000000,
	    0x3076168300000000,0xDCBFEBDBC0000000,0x5BF6AF2740000000,0x3D3BC1A4E0000000,
	    0xB80A554140000000,0xF2E8677D30000000,0xEC7EA4B6A0000000,0xF561F463A0000000,
	    0xF40163ED48000000,0x58AD0A4AB0000000,0xAD1DE061CA000000,0x3257E8CF8C000000,
	    0xFECD615F0C000000,0x9DCCF1A37E000000,0x4FEB7ABE7E000000};

uint32_t random;
int rndm_indx = 0;

void debug_print(uint64_t a) {
	#ifdef MY_DEBUG
	printf("%16llX: ", a);
	for (int i = 0; i < 64; ++i) {
		uint64_t index = ((uint64_t)1) << 63-i;
		if (0 == (i % 4)) printf("|");
		if (0 == (i % 16)) printf("|");
		if (index & a) 
			printf("1");
		else
			printf("0"); 
	}
	printf("\n");
	#endif
}

/**
 * @brief      reads 4 randombytes and saves them in an 32-bit int
 *
 * @return     { description_of_the_return_value }
 */
void new_random() {
	randombytes((unsigned char*)&random, 4);
}

/* exists for testing purposes
//so we get higher samples each time and can test the border cases
int random_bit() {
	if (rand() > 0.9*RAND_MAX) return 0;
	return 1;
}
*/

int random_bit() {
	if (rndm_indx == 32) {
		new_random();
		rndm_indx = 0;
	}
	uint32_t index = ((uint32_t)1) << rndm_indx++;
	if (index & random) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * @brief      samples a gaussian based on the given probability matrix
 *
 * @note       { with very low probability this generates an exception value +/- (TAILCUT+1). }
 *
 * @return     { sampled value from [-TAILCUT, TAILCUT] }
 */
int32_t knuth_yao_sample() {

	uint64_t d = 0;
	int col = 0;
	int32_t row;
	uint64_t index;

	while (1) {
		d = 2*d + random_bit();
		#ifdef MY_DEBUG
		printf("\n");
		debug_print(d);
		debug_print(prob_matrix[col]);
		printf("\n");
		#endif

		//this for loop cycles one bit from the 38th bit to the 0th bit to achieve bit-wise access
		//to the prob_matrix
		for (row = TAILCUT+1; row >= 0; row--) {
			
			index = ((uint64_t)1) << 63-row;

			debug_print(index);
			debug_print(prob_matrix[col] & index);

			if (prob_matrix[col] & index) d--;
			if (-1 == d) {
				//return sample or additive inverse of sample depending on a random_bit
				if (random_bit() && 0 != row)
					return -row;
				else 
					return row;
			}
		}
		col++;
	}
}
/**
 * @brief      This produces a "small" error term from the defined error distribution
 *
 * @return     { a poly sampled from a multivariate gaussian }
 */
poly sample_from_dist() {

	poly result;
	int32_t tmp;
	FOR(i,n) {
		// This is necessary because with very low probability knuth_yao samples an out of bound.
		while (abs(tmp = knuth_yao_sample()) > MAX_GAUSS) {
		}
		result.coeff[i] = tmp;
	}
	return result;
}


/**
 * @brief      produces an (almost) uniform poly from the Ring R_q
 *
 * @return     {sampled polynomial with all coefficients \in {0..q-1}}
 */
poly sample_from_ring() {
	poly result;
	uint16_t tmp;
	FOR(i,n) {
		randombytes((unsigned char*)&tmp, 2);
		result.coeff[i] = tmp % MOD;
	}
	return result;
}

#ifdef MY_DEBUG

#include <stdio.h>

void print_poly(poly a, char *message) {
	if (message != "") {
		printf("%s\n", message);
	}

	FOR(i,n) {
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}

void print_tuple(tuple a, char *message) {

	if (message != "") {
		printf("%s\n", message);
	}
	printf(".a:\n");
	FOR(i,n) {
		printf("%4d|", a.a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n.b:\n");
	FOR(i,n) {
		printf("%4d|", a.b.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");

}

#endif

/**
 * @brief Generation algorithm for public key pair
 * 
 * This algorithm generates a public key pair (sk: secret key, pk: public key)
 * for the use with 256_ring_lwe_suit.
 * 
 */
void crypto_generate_key_pair(poly *sk, tuple *pk) {

#ifdef MY_DEBUG

printf("\n\n*****************************************************************************************************************************\nGeneration:\n");

#endif

	*sk = sample_from_dist();
	(*pk).a = sample_from_ring();
	(*pk).b = __add_poly(__mult_poly((*pk).a, *sk), sample_from_dist());

#ifdef MY_DEBUG

print_poly(*sk, "Secret key:");
print_tuple(*pk, "Public key:");
printf("\n\nEnd Generation\n*****************************************************************************************************************************\n");

#endif
}

/**
 * @brief      Encryption function
 *
 * @param[in]  pk       { public key }
 * @param[in]  message  { poly with entries 0 or 1 }
 *
 * @return     { cipher }
 */
tuple crypto_box(tuple pk, poly message) {

#ifdef MY_DEBUG

printf("\n\n*****************************************************************************************************************************\nEncryption:\n");
printf("input:\n");
print_tuple(pk, "Public Key:");
print_poly(message, "Message:");
printf("\nEncrypting...\n");

#endif

	int half = (int) floor(MOD/2);
	FOR(i,n) message.coeff[i] = message.coeff[i] * half;

#ifdef MY_DEBUG

printf("Rounding message bytes to %d...\n", half);
print_poly(message, "New message:");

#endif
	
	poly r = sample_from_dist();
	poly e1 = sample_from_dist();
	poly e2 = sample_from_dist();

#ifdef MY_DEBUG

printf("Sampling from distribution...\n");
print_poly(r, "r:");
print_poly(e1, "e1:");
print_poly(e2, "e2:");
printf("Calculating...\n");

#endif

	tuple res;

	res.a = __add_poly(e1, __mult_poly(pk.a, r));
	res.b = __add_poly(message, __add_poly(e2, __mult_poly(pk.b, r)));

#ifdef MY_DEBUG

print_tuple(res, "Output:");
printf("Encryption done.\n");

printf("\n\nEnd Encryption\n*****************************************************************************************************************************\n");
#endif

	return res;
}

poly crypto_unbox(tuple cipher, poly sk) {

#ifdef MY_DEBUG

printf("\n\n*****************************************************************************************************************************\nDecryption:\n");
printf("Decryption:\ninput:\n");
print_tuple(cipher, "Cipher:");
print_poly(sk, "Secret key:");
printf("\nDecrypting...\n");
printf("Calculating...\n");

#endif
	
	poly res = __add_poly(cipher.b, __add_inverse(__mult_poly(cipher.a, sk)));

#ifdef MY_DEBUG

print_poly(res, "Unrounded:");

#endif

	int lower_bound = (int) floor(MOD/4);
	int upper_bound = (int) floor(3*MOD/4);

	FOR(i,n) res.coeff[i] = ((lower_bound < res.coeff[i]) && 
		(res.coeff[i] <= upper_bound))?1:0;

#ifdef MY_DEBUG

printf("Rounding interval: [%4d, %4d]\nDecryption done.\n", lower_bound, 
	upper_bound);
print_poly(res, "Output:");
printf("\n\nEnd Decryption\n*****************************************************************************************************************************\n");

#endif

	return res;

}