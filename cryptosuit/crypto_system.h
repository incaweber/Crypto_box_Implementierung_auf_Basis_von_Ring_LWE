#ifndef CRYPTOSYSTEM
#define CRYPTOSYSTEM
void crypto_generate_key_pair(poly *sk, tuple *pk);
tuple crypto_box(tuple pk, poly message);
poly crypto_unbox(tuple cipher, poly sk);
#endif