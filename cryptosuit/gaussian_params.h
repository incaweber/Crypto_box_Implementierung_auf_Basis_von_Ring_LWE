#ifndef GAUSS
#define GAUSS
#define TAILCUT 37
#define GAUSSIAN 11.31
#define MANTISSA 52
#define BIAS 1023
#define EXPONENT 0x07FF
#define PRECISION 55
#define MAX_GAUSS 30
#endif