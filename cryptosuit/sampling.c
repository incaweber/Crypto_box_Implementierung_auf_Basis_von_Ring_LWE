#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include "gaussian_params.h"
#include "types.h"
#include "randombytes.h"

/**
 * This is the columnwise representation of the probability matrix. It consits of 
 * PRECISION (gaussian_params.h) 64-bit integers that each represent one column.
 * We process it bit-wise. Each bit represents a terminal node in the DDG-Tree of
 * the Knuth-Yao Algorithm. The deepest bit used in every int is TAILCUT 
 * (gaussian_params.h)
 */
uint64_t prob_matrix[PRECISION] = 
	   {0x0000000000000000,0x0000000000000000,0x7000000000000000,0x8E00000000000000,
	    0x6D80000000000000,0xD940000000000000,0x8A70000000000000,0x21A8000000000000,
	    0x8B24000000000000,0x14BA000000000000,0xC6E5000000000000,0x075E800000000000,
	    0x7C03400000000000,0x0D87A00000000000,0xDFA7000000000000,0x4E58D00000000000,
	    0xF594D80000000000,0x5C08000000000000,0x60C91C0000000000,0x0166460000000000,
	    0x2520A40000000000,0xBCD5E30000000000,0x0612358000000000,0x0B67C00000000000,
	    0xFED9BD4000000000,0x570F430000000000,0xF5850EE000000000,0x5F6D700000000000,
	    0x100F563000000000,0x31C5A43000000000,0x9214E56800000000,0x9698E2C800000000,
	    0x9D3490F400000000,0xB65AC52C00000000,0x8F5BFC5200000000,0x45CFF65600000000,
	    0x04D1B37900000000,0x42AE5C4800000000,0x796A66F180000000,0x520EC2B000000000,
	    0x3076168300000000,0xDCBFEBDBC0000000,0x5BF6AF2740000000,0x3D3BC1A4E0000000,
	    0xB80A554140000000,0xF2E8677D30000000,0xEC7EA4B6A0000000,0xF561F463A0000000,
	    0xF40163ED48000000,0x58AD0A4AB0000000,0xAD1DE061CA000000,0x3257E8CF8C000000,
	    0xFECD615F0C000000,0x9DCCF1A37E000000,0x4FEB7ABE7E000000};

uint32_t random;
int rndm_indx = 0;

void debug_print(uint64_t a) {
	#ifdef MY_DEBUG
	printf("%16llX: ", a);
	for (int i = 0; i < 64; ++i) {
		uint64_t index = ((uint64_t)1) << 63-i;
		if (0 == (i % 4)) printf("|");
		if (0 == (i % 16)) printf("|");
		if (index & a) 
			printf("1");
		else
			printf("0"); 
	}
	printf("\n");
	#endif
}

/**
 * @brief      reads 4 randombytes and saves them in an 32-bit int
 *
 * @return     { description_of_the_return_value }
 */
void new_random() {
	randombytes((unsigned char*)&random, 4);
}

/* exists for testing purposes
//so we get higher samples each time and can test the border cases
int random_bit() {
	if (rand() > 0.9*RAND_MAX) return 0;
	return 1;
}
*/

int random_bit() {
	if (rndm_indx == 32) {
		new_random();
		rndm_indx = 0;
	}
	uint32_t index = ((uint32_t)1) << rndm_indx++;
	if (index & random) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * @brief      samples a gaussian based on the given probability matrix
 *
 * @note       { with very low probability this generates an exception value +/- (TAILCUT+1). }
 *
 * @return     { sampled value from [-TAILCUT, TAILCUT] }
 */
int32_t knuth_yao_sample() {

	uint64_t d = 0;
	int col = 0;
	int32_t row;
	uint64_t index;

	while (1) {
		d = 2*d + random_bit();
		#ifdef MY_DEBUG
		printf("\n");
		debug_print(d);
		debug_print(prob_matrix[col]);
		printf("\n");
		#endif

		//this for loop cycles one bit from the 38th bit to the 0th bit to achieve bit-wise access
		//to the prob_matrix
		for (row = TAILCUT+1; row >= 0; row--) {
			
			index = ((uint64_t)1) << 63-row;

			debug_print(index);
			debug_print(prob_matrix[col] & index);

			if (prob_matrix[col] & index) d--;
			if (-1 == d) {
				//return sample or additive inverse of sample depending on a random_bit
				if (random_bit() && 0 != row)
					return -row;
				else 
					return row;
			}
		}
		col++;
	}
}
/**
 * @brief      This produces a "small" error term from the defined error distribution
 *
 * @return     { a poly sampled from a multivariate gaussian }
 */
poly sample_from_dist() {

	poly result;
	int32_t tmp;
	FOR(i,n) {
		// This is necessary because with very low probability knuth_yao samples an out of bound.
		while (abs(tmp = knuth_yao_sample()) > MAX_GAUSS) {
		}
		result.coeff[i] = tmp;
	}
	return result;
}


/**
 * @brief      produces an (almost) uniform poly from the Ring R_q
 *
 * @return     {sampled polynomial with all coefficients \in {0..q-1}}
 */
poly sample_from_ring() {
	poly result;
	uint16_t tmp;
	FOR(i,n) {
		randombytes((unsigned char*)&tmp, 2);
		result.coeff[i] = tmp % MOD;
	}
	return result;
}