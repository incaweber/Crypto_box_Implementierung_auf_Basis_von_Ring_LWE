#include "types.h"
#include "ntt.h"
#include <math.h>

//#define MY_DEBUG

#ifdef MY_DEBUG

#include <stdio.h>

void print_poly(poly a) {
	FOR(i,n) {
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}

void print_double_poly(double_poly a) {
	for (int i = 0; i < DOUBLE_DIM; ++i)
	{
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}

#endif

/**
 * @brief      Modulo function with fixed modulus MOD (@see types.h)
 * 
 * Warning this is really inefficient. Only for use of very close numbers.
 * This is only used in subtractions of field elements.
 *
 * @param[in]  a     { value to calculate modulus of }
 *
 * @return     { returns representative 0 <= x < MOD of the coset of a }
 */
int32_t modulo(int32_t a) {

	while (a < 0) {
		a += MOD;
	}

	a = a % MOD;

	return a;
}


/**
 * @brief      naive implementation of modular exponentiation
 *
 * @param[in]  a     { base }
 * @param[in]  b     { exponent }
 *
 * @return     { return a^b mod MOD (@see types.h) }
 */
int32_t power(int32_t a, int32_t b) {

	if (b == 0) return 1;
	if (b == 1) return a;

	int x = a;
	//calculates a^b mod c
	while (--b > 0) {
		x = x*a % MOD;
	}

	return x;
}

/**
 * @brief      Ringaddition (Z_MOD^DIM[x]/(x^DIM+1))
 *
 * @param[in]  x1    summand 1
 * @param[in]  x2    summand 2
 *
 * @return     { sum of x1 and x2 modul }
 */
poly __add_poly(poly x1, poly x2) {
	poly res;
	FOR(i,n) res.coeff[i] = (x1.coeff[i] + x2.coeff[i]) % MOD;
	return res;
}

/**
 * @brief      Ringmultiplication (Z_MOD^DIM[x]/(x^DIM+1))
 *
 * Multiplicates two ring elements x1 and x2 using NTT that evaluates the 
 * polynomials in 2*DIM points - the roots of unity -, multiplicates them
 * component-wise then transforms them back and reduces them back to the ring.
 *
 * @param[in]  x1    factor 1
 * @param[in]  x2    factor 2
 *
 * @return     { product (in the ring of x1 and x2) }
 */
poly __mult_poly(poly x1, poly x2) {
	
#ifdef MY_DEBUG

printf("\n__mult_poly with: x1,x2:\n");
print_poly(x1);
print_poly(x2);

#endif

	poly result;
	double_poly ntt1;
	double_poly ntt2;
	double_poly ntt_mul; 

	//padding with zeroes for multiplication:
	for (int i = 0; i < DOUBLE_DIM; ++i) {
		ntt1.coeff[i] = (i<DIM)?0:x1.coeff[i-DIM];
		ntt2.coeff[i] = (i<DIM)?0:x2.coeff[i-DIM];
	}

#ifdef MY_DEBUG

printf("\nPadded x1,x2:\n");
print_double_poly(ntt1);
print_double_poly(ntt2);

#endif

	//NTT-Transformation
	ntt1 = double_ntt(ntt1);
	ntt2 = double_ntt(ntt2);

#ifdef MY_DEBUG

printf("\nTransformed x1,x2:\n");
print_double_poly(ntt1);
print_double_poly(ntt2);

#endif


	//ntt does not give ring elements in the same ring with double dimension
	//so no negative values => can use built in mod
	for (int i = 0; i < DOUBLE_DIM; ++i) {
		ntt_mul.coeff[i] = (ntt1.coeff[i] * ntt2.coeff[i]) % MOD;
	}

#ifdef MY_DEBUG

printf("\nCoefficient-wise multiplication:\n");
print_double_poly(ntt_mul);

#endif

	//Inverse NTT-Transformation
	ntt_mul = double_inv_ntt(ntt_mul);

#ifdef MY_DEBUG

printf("\nInverse transformed:\n");

print_double_poly(ntt_mul);

#endif

	//reducing result mod (x^MOD + 1)
	//NOTE: the multiplication puts the leading zero we have to ignore
	//to the last index (DOUBLE_DIM-1)
	result.coeff[0] = ntt_mul.coeff[DIM-1];
	for (int i = 0; i < DIM-1; ++i) 
		result.coeff[i+1] = modulo(ntt_mul.coeff[i+DIM] - ntt_mul.coeff[i]);

#ifdef MY_DEBUG

printf("\nReduced:\n");
print_poly(result);

#endif

	return result;
}

/**
 * @brief      additional inverse
 * 
 * Calculates the additional inverse of a ring element, s.t.: 
 * __add_poly(x1, __add_inverse(x1)) = [0].
 *
 * @param[in]  x1    the poly to invert
 */
poly __add_inverse(poly x1) {

	poly res;
	FOR(i,n) res.coeff[i] = (x1.coeff[i] == 0)?0:MOD - x1.coeff[i];
	return res;
}