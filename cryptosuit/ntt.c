#include "types.h"
#include "functions.h"
#include <math.h>
#include <stdio.h>


/**
 * @brief      recursion helper for NTT calculation of a poly
 *
 * @param      result  { gets step-by-step filled with the evaluated polynomial}
 * @param      a       { polynomial to calculate ntt of }
 * @param[in]  n       { arraysizes }
 */
void forward_recursive_ntt(int32_t result[], int32_t a[], int n) {

	//base case:
	if (n == 1) {
		result[0] = a[0];
		return;
	}

	//calculate the n-th root of unity.
	//note that (MOD-1)/n is always an int because by requirement we have that:
	//MOD = 1 mod 2*DIM
	//and DIM is a power of 2
	int32_t root = power(GEN, (int32_t)(MOD-1)/n);
	int32_t omega = 1;


	//split a into two pieces
	//where a0 contains all even indices
	//and a1 contains all odd indices	
	int half = n/2;
	int32_t a0[half];
	int32_t a1[half];
	int32_t res0[half];
	int32_t res1[half];

	for (int i = 0; i < n; ++i) {
		int tmp = ceil(i/2);
		if (i % 2) {
			a1[tmp] = a[i];
		} else {
			a0[tmp] = a[i];
		}
	}

	//recursive case:
	forward_recursive_ntt(res0, a0, half);
	forward_recursive_ntt(res1, a1, half);

	//coming out of the recursion and doing the calculations
	for (int i = 0; i < half; ++i) {
		result[i] = (res0[i] + omega*res1[i]) % MOD;
		result[i+half] = modulo(res0[i] - ((omega*res1[i]) % MOD));
		omega = omega*root % MOD;
	}
}

/**
 * @brief      recursion helper for calculating the inverse ntt
 *
 * @param      result  { gets step-by-step filled with polynomial }
 * @param      a       { polynomial to calculate inverse ntt of }
 * @param[in]  n       { arraysizes }
 */
void inverse_recursive_ntt(int32_t result[], int32_t a[], int n) {

	//base case:
	if (n == 1) {
		result[0] = a[0];
		return;
	}

	//calculate the inverse of the n-th root of unity.
	//note that a^-1 = a^(n-1)
	int32_t root = power(GEN, (int32_t)(MOD-(MOD-1)/n)-1);
	int32_t omega = 1;

	//split a into two pieces 
	//where a0 contains all even indices
	//and a1 contains all odd indices
	int half = n/2;
	int32_t a0[half];
	int32_t a1[half];
	int32_t res0[half];
	int32_t res1[half];

	for (int i = 0; i < n; ++i) {
		int tmp = ceil(i/2);
		if (i % 2) {
			a1[tmp] = a[i];
		} else {
			a0[tmp] = a[i];
		}
	}

	//recursive case:
	inverse_recursive_ntt(res0, a0, half);
	inverse_recursive_ntt(res1, a1, half);

	//coming out of the recursion and doing the calculations
	for (int i = 0; i < half; ++i) {				
		result[i] = (res0[i] + omega*res1[i]) % MOD;
		result[i+half] = modulo(res0[i] - ((omega*res1[i]) % MOD));
		omega = omega*root % MOD;
	}
}

poly ntt(poly a) {

	poly res;
	forward_recursive_ntt(res.coeff, a.coeff, DIM);
	return res;
}

poly inv_ntt(poly a) {

	poly res;
	inverse_recursive_ntt(res.coeff, a.coeff, DIM);
	FOR(i,n) res.coeff[i] = res.coeff[i] * INV % MOD;
	return res;
}

double_poly double_ntt(double_poly a) {

	double_poly res;
	forward_recursive_ntt(res.coeff, a.coeff, DOUBLE_DIM);
	return res;
}

double_poly double_inv_ntt(double_poly a) {

	double_poly res;
	inverse_recursive_ntt(res.coeff, a.coeff, DOUBLE_DIM);
	for (int i = 0; i < DOUBLE_DIM; ++i) 
		res.coeff[i] = (res.coeff[i] * DOUBLE_INV) % MOD;
	return res;
}
