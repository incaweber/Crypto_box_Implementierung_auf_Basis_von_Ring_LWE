#ifndef NTT
#define NTT
poly inv_ntt(poly a);
poly ntt(poly a);
double_poly double_ntt(double_poly a);
double_poly double_inv_ntt(double_poly a);
#endif