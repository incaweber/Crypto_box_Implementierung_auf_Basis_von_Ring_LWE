/**
 * This ist copied from the NaCl library from Daniel J. Bernstein, et. al.
 * Source: https://nacl.cr.yp.to/
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

static int fd = -1;
/**
 * @brief      { reads random bytes from the OS }
 *
 * @param      x     { string where the random bytes are saved }
 * @param[in]  xlen  amount of bytes to read (max 2^31)
 */
void randombytes(unsigned char *x, unsigned long long xlen)
{
  int i;

  if (fd == -1) {
    for (;;) {
      fd = open("/dev/urandom",O_RDONLY);
      if (fd != -1) {
      	break;
      }
      sleep(1);
    }
  }

  while (xlen > 0) {
    if (xlen < 1048576) i = xlen; else i = 1048576;

    i = read(fd,x,i);
    if (i < 1) {
      sleep(1);
      continue;
    }

    x += i;
    xlen -= i;
  }
}
