#ifndef FUNCTIONS
#define FUNCTIONS
int32_t modulo(int32_t a);
int32_t power(int32_t a, int32_t b);
poly __add_poly(poly x1, poly x2);
poly __mult_poly(poly x1, poly x2);
poly __add_inverse(poly x1);
poly __mult_inverse(poly x1);
#endif