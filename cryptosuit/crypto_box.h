#ifndef TYPES 
#define TYPES

#include <inttypes.h>

#define MOD 7681 /* modulus of the ring */
#define GEN 17   /* generator in the ring */

//NOTE: only edit these together!
#define DOUBLE_DIM 512  /* two times dimension of the ring */
#define DOUBLE_INV 7666 /* multiplicative inverse of the doubled dimension modulo MOD */
#define DIM 256  /* dimension of the ring */
#define INV 7651 /* multiplicative inverse of the dimension modulo MOD */

/**
 * @brief 	   {holds one ring element (private key is a ring element)}
 */
typedef struct poly { int32_t coeff[DIM]; } poly;

/**
 * @brief      {holds a public key of the form: (a, b = a*s+e)}
 */
typedef struct tuple {
	poly a;
	poly b;
} tuple;

#endif

void crypto_generate_key_pair(poly *sk, tuple *pk);
tuple crypto_box(tuple pk, poly message);
poly crypto_unbox(tuple cipher, poly sk);