#include <stdio.h>


int main(int argc, char const *argv[])
{
	printf("long: %d bytes\n", sizeof(long));
	printf("int: %d bytes\n", sizeof(int));
	printf("long int: %d bytes\n", sizeof(long int));
	printf("float: %d bytes\n", sizeof(float));
	printf("double: %d bytes\n", sizeof(double));
	printf("long long: %d bytes\n", sizeof(long long));
	return 0;
}