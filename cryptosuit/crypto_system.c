/**
 * @author     Inca Max Weber
 *
 * @brief      Implementation of crypto-suite based on ring-LWE
 *
 *             This Implementation is made best-effort. The parameter set used is (n,q,s) : 
 *             (256, 7681, 11.31). Like this we get the Ring 
 *             R^256 = Z^256_7681[x]/(x^256+1)
 */

#include "types.h"
#include "functions.h"
#include "sampling.h"
#include <math.h>

//#define MY_DEBUG

#ifdef MY_DEBUG

#include <stdio.h>

void print_poly(poly a, char *message) {
	if (message != "") {
		printf("%s\n", message);
	}

	FOR(i,n) {
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}

void print_tuple(tuple a, char *message) {

	if (message != "") {
		printf("%s\n", message);
	}
	printf(".a:\n");
	FOR(i,n) {
		printf("%4d|", a.a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n.b:\n");
	FOR(i,n) {
		printf("%4d|", a.b.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");

}

#endif

/**
 * @brief Generation algorithm for public key pair
 * 
 * This algorithm generates a public key pair (sk: secret key, pk: public key)
 * for the use with 256_ring_lwe_suit.
 * 
 */
void crypto_generate_key_pair(poly *sk, tuple *pk) {

#ifdef MY_DEBUG

printf("\n\n*****************************************************************************************************************************\nGeneration:\n");

#endif

	*sk = sample_from_dist();
	(*pk).a = sample_from_ring();
	(*pk).b = __add_poly(__mult_poly((*pk).a, *sk), sample_from_dist());

#ifdef MY_DEBUG

print_poly(*sk, "Secret key:");
print_tuple(*pk, "Public key:");
printf("\n\nEnd Generation\n*****************************************************************************************************************************\n");

#endif
}

/**
 * @brief      Encryption function
 *
 * @param[in]  pk       { public key }
 * @param[in]  message  { poly with entries 0 or 1 }
 *
 * @return     { cipher }
 */
tuple crypto_box(tuple pk, poly message) {

#ifdef MY_DEBUG

printf("\n\n*****************************************************************************************************************************\nEncryption:\n");
printf("input:\n");
print_tuple(pk, "Public Key:");
print_poly(message, "Message:");
printf("\nEncrypting...\n");

#endif

	int half = (int) floor(MOD/2);
	FOR(i,n) message.coeff[i] = message.coeff[i] * half;

#ifdef MY_DEBUG

printf("Rounding message bytes to %d...\n", half);
print_poly(message, "New message:");

#endif
	
	poly r = sample_from_dist();
	poly e1 = sample_from_dist();
	poly e2 = sample_from_dist();

#ifdef MY_DEBUG

printf("Sampling from distribution...\n");
print_poly(r, "r:");
print_poly(e1, "e1:");
print_poly(e2, "e2:");
printf("Calculating...\n");

#endif

	tuple res;

	res.a = __add_poly(e1, __mult_poly(pk.a, r));
	res.b = __add_poly(message, __add_poly(e2, __mult_poly(pk.b, r)));

#ifdef MY_DEBUG

print_tuple(res, "Output:");
printf("Encryption done.\n");

printf("\n\nEnd Encryption\n*****************************************************************************************************************************\n");
#endif

	return res;
}

poly crypto_unbox(tuple cipher, poly sk) {

#ifdef MY_DEBUG

printf("\n\n*****************************************************************************************************************************\nDecryption:\n");
printf("Decryption:\ninput:\n");
print_tuple(cipher, "Cipher:");
print_poly(sk, "Secret key:");
printf("\nDecrypting...\n");
printf("Calculating...\n");

#endif
	
	poly res = __add_poly(cipher.b, __add_inverse(__mult_poly(cipher.a, sk)));

#ifdef MY_DEBUG

print_poly(res, "Unrounded:");

#endif

	int lower_bound = (int) floor(MOD/4);
	int upper_bound = (int) floor(3*MOD/4);

	FOR(i,n) res.coeff[i] = ((lower_bound < res.coeff[i]) && 
		(res.coeff[i] <= upper_bound))?1:0;

#ifdef MY_DEBUG

printf("Rounding interval: [%4d, %4d]\nDecryption done.\n", lower_bound, 
	upper_bound);
print_poly(res, "Output:");
printf("\n\nEnd Decryption\n*****************************************************************************************************************************\n");

#endif

	return res;

}