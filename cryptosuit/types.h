#ifndef TYPES
#define TYPES
#define MOD 7681 /* modulus of the ring */
#define GEN 17   /* generator in the ring */

//NOTE: only edit these together!
#define DOUBLE_DIM 512  /* two times dimension of the ring */
#define DOUBLE_INV 7666 /* multiplicative inverse of the doubled dimension modulo MOD */
#define DIM 256  /* dimension of the ring */
#define INV 7651 /* multiplicative inverse of the dimension modulo MOD */


#include <inttypes.h>


/**
 * @brief 	   {holds one ring element (private key is a ring element)}
 */
typedef struct poly { int32_t coeff[DIM]; } poly;

/**
 * @brief      { holds ring element with double dimension.
 * 				 this is used for elementwise multiplication with ntt.
 * 				 The polynomials get padded with zeros in the front in a 
 * 				 way that allows us to calculate the 2n-1 sized product of
 * 				 two polynomials }
 */
typedef struct double_poly { int32_t coeff[2*DIM]; } double_poly;

/**
 * @brief      {holds a public key of the form: (a, b = a*s+e)}
 */
typedef struct tuple {
	poly a;
	poly b;
} tuple;

#define FOR(i,n) for (int i = 0; i < DIM; ++i)

#endif