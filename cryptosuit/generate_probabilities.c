#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include "gaussian_params.h"

uint64_t probabilities[PRECISION];


void print_binary_double(int d, double x) {
	//works only for negative exponents

	int i;
	uint64_t a = *(uint64_t*)&x;

	//prettify print:
	printf("%2d: 0.", d);

	//extract the exponent:
	int b = (a >> MANTISSA) & EXPONENT;
	b = abs(b-BIAS);
	for (i = 0; i < b-1; ++i)
	{
		printf("0");
	}

	printf("1"); //print implicit 1

	for (i = MANTISSA-1; i >= b + MANTISSA-(PRECISION-1); --i) {
		printf("%u", (int)(a >> i) & 0x01);
	}

	printf("\n");

}


void print_double(int d, double z) {

	printf("%2d: %1.10f\n", d, z);

}

void put_in_array(int d, double x) {
	//works only for negative exponents

	int i,j;
	uint64_t index = ((uint64_t)1) << 63-d;				//contains only a 1 at the d-th bit
	uint64_t a = *(uint64_t*)&x;

	//handle exponent:
	int b = (a >> MANTISSA) & EXPONENT;
	b = abs(b-BIAS);
	//no need to insert the leading 0s, they are already all 0 by initialization
	
	//don't forget the implicit 1 at the b-th array entry
	probabilities[b-1] |= index;

	j = b;
	//handle mantissa:
	for (i = MANTISSA-1; i >= b + MANTISSA-(PRECISION); --i,j++) {
		// if ((a >> i) & 0x01) == 1 we need to write to position: j
		if ((a >> i) & 0x01) probabilities[j] |= index;
	}
}

void print_probabilities() {

	int t,p;
	uint64_t index;
	for (t = 0; t <= TAILCUT; t++) {
		printf("%2d: 0.", t);
		index = ((uint64_t)1) << 63-t;
		for (p = 0; p < PRECISION; p++)
		{
			(probabilities[p] & index)? printf("1"):printf("0");
		}
		printf("\n");
	}
	// !!!!! NOTE: In this example this was the rest we were missing to a total probability of 1
	//This has to be added to the matrix manually
	// !!!!! NOTE: If anything changes in the code this has to be recalculated.
	printf("XX: 0.0000000000000000000000000000000000000000000000000010011\n");

}

void print_probabilities_as_array() {
	int p;
	printf("{");
	for (p = 0; p < PRECISION-1; p++)
	{
		printf("0x%016" PRIX64 ",", probabilities[p]);
		if (3 == p % 4) printf("\n ");
	}
	printf("0x%016" PRIX64 "}\n", probabilities[p]);
}



int main(int argc, char const *argv[]) {

	int i;

	//set probabilities to 0:
	for (i = 0; i < PRECISION; ++i)
	{
		probabilities[i] = 0;
	}

	double z;
	double pi = 3.141592653589793115997963468544185161590576171875; 	//double precision
	double e = 2.718281828459045090795598298427648842334747314453125;	//double precision
	double s = GAUSSIAN;				//s = \sigma*sqrt(2*pi) ;; is our gaussian parameter
	
	for (i = 0; i <= TAILCUT; i++) {
	
		z = ((i*i)*(-pi))/(s*s);
		z = pow(e,z)/s;
	
		if (i != 0) z *= 2;
	
		// print_double(i,z);
		// print_binary_double(i,z);
		put_in_array(i,z);
	}
	print_probabilities();
	print_probabilities_as_array();
	return 0;
}

