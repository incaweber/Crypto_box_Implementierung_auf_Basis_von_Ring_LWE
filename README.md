The high level functions to use are in crypto_box.c.

For usage:

1) include crypto_box.h.

2) use crypto_generate_key_pair, crypto_box, crypto_unbox

3) make sure randombytes.c and -.h are available (part of the NaCl-library)

4) compile with gcc -std=c99 (crypto_box_path) (randombytes_path) -lm