#include "../cryptosuit/types.h"
#include "../cryptosuit/sampling.h"
#include "../cryptosuit/gaussian_params.h"


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void print_poly(poly a) {
	FOR(i,n) {
		printf("%4d|", (int16_t)a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}


int main(int argc, char const *argv[]) {

	poly a = sample_from_ring();
	print_poly(a);

	poly b = sample_from_dist();
	print_poly(b);

	return 0;
}