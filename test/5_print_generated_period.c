#include "../cryptosuit/types.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define GENERATOR 3449

int power(int a, int b, int c) {

	if (b == 0) return 1;
	if (b == 1) return a;

	int x = a;
	//calculates a^b mod c
	while (--b > 0) {
		x = x*a % c;
	}

	return x;
}

int main(int argc, char const *argv[]) {

	int gen = (argc > 1)?atoi(argv[1]):GENERATOR;

	int results[MOD];
	int flow = 0;
	int i,j;
	for (i = 0; i < MOD; i++) {

		results[i] = power(gen,i,MOD);

		for (j = 0; j < i; j++) {
			if (results[i] == results[j]) {
				flow = 1;
				break;
			}
		}

		if (flow) break;

	}

	printf("[");

	for (j = 0; j < i; j++) {

		printf("%4d,", results[j]);

	}

	printf("%4d]\n", results[j]);

	return 0;
}