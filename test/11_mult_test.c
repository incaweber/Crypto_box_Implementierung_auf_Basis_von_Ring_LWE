#include "../cryptosuit/types.h"
#include "../cryptosuit/functions.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char const *argv[]) {

	poly a,b,c;
	FOR(i,n) {
		a.coeff[i] = 1;
		b.coeff[i] = 1;
	}
	c = __mult_poly(a,b);

	return 0;
}