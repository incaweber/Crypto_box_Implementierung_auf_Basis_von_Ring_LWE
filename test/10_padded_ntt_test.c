#include "../cryptosuit/types.h"
#include "../cryptosuit/sampling.h"
#include "../cryptosuit/ntt.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void myprint_poly(double_poly a) {
	for (int i = 0; i < DOUBLE_DIM; ++i) {
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}

int main(int argc, char const *argv[]) {

	double_poly d;
	poly a = sample_from_ring();
	for (int i = 0; i < DOUBLE_DIM; ++i) {
		 d.coeff[i] = (i < DIM)?0:1;
	}
	myprint_poly(d);
	d = double_ntt(d);
	myprint_poly(d);
	d = double_inv_ntt(d);
	myprint_poly(d);

	return 0;
}
