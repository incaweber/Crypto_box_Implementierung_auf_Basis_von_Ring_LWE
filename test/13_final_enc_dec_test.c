#include "../cryptosuit/types.h"
#include "../cryptosuit/crypto_system.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void my_print_poly(poly a) {
	FOR(i,n) {
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}


int main(int argc, char const *argv[]) {

	poly s_key;
	tuple p_key;
	poly message;
	FOR(i,n) message.coeff[i] = (i%2)?1:0;

	crypto_generate_key_pair(&s_key, &p_key);

	printf("\n\nSecret Key:\n");
	my_print_poly(s_key);
	printf("\n\nPublic Key:\n");
	my_print_poly(p_key.a);
	printf("\n");
	my_print_poly(p_key.b);

	poly decrypted = crypto_unbox(crypto_box(p_key, message), s_key);

	printf("\nOriginal Message:\n");
	my_print_poly(message);
	printf("\nDecrypted Message:\n");
	my_print_poly(decrypted);

	// void generate_key_pair(poly *sk, tuple *pk);
	// tuple encryption(tuple pk, poly message);
	// poly decryption(tuple cipher, poly sk);


	return 0;
}