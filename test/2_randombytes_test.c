#include "../cryptosuit/randombytes.h"
#include <stdio.h>

int main(int argc, char const *argv[]) {

	char x[4];

	randombytes(x,4);

	printf("%X\n", *(unsigned int*)&x);
	
	return 0;
}