#include "../cryptosuit/types.h"
#include "../cryptosuit/crypto_system.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

void print_poly(poly a) {
	FOR(i,n) {
		printf("%4d|", a.coeff[i]);
		if (24 == i%25) printf("\n");
	}
	printf("\n");
}


int main(int argc, char const *argv[]) {

	poly s_key;
	tuple p_key;

	generate_key_pair(&s_key, &p_key);

	print_poly(s_key);
	print_poly(p_key.a);
	print_poly(p_key.b);


	return 0;
}