#include "../cryptosuit/types.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int power(int a, int b) {

	if (b == 0) return 1;
	if (b == 1) return a;

	int x = a;
	
	while (--b > 0) {
		x = x*a % MOD;
	}

	return x;
}

int main(int argc, char const *argv[]) {

	int n = DIM;

	while (n > 1) {
		printf("%3d: %4d : %4d\n", n, power(GEN, (int16_t)(MOD-1)/n), power(GEN, (int16_t)(MOD-(MOD-1)/n)-1));
		n = (int) (n/2);
	}
	printf("%d\n", power(GEN, MOD-1));

	return 0;
}