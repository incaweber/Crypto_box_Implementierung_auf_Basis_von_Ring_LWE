#include "../cryptosuit/types.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int results[7681];

void print_results(FILE *fp, int max) {
	fprintf(fp, "[");
	for (int i = 0; i < max; ++i)
	{
		fprintf(fp, "%4d, ", results[i]);
		if (i % 20 == 19) fprintf(fp, "\n");
	}
	fprintf(fp, "%4d]\n", results[max]);
}

int power(int a, int b, int c) {

	if (b == 0) return 1;
	if (b == 1) return a;

	int x = a;

	//calculates a^b mod c
	while (b > 1) {
		x = x*a % c;
		b--;
	}

	return x;
}

int main(int argc, char const *argv[]) {

	FILE *fp;
	int i;
    fp = fopen ("generated_sequences.txt","w");
    if (fp == NULL) {
        printf ("File not created okay.\n");
        return 1;
    }
	int flow = 0;

	for (int k = 2; k < q; k++) {

		printf("%d \n", k);

		fprintf(fp, "*** %d :\n", k);
		flow = 0;

		for (i = 0; i < q-1; i++) {

			results[i] = power(k,i,q);

			for (int j = 0; j < i; j++) {
				if (results[i] == results[j]) {
					flow = 1;
					break;
				}
			}

			if (flow) break;

		}
		if (i == q-1) {
			print_results(fp, i);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
	return 0;
}